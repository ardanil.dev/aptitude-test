package com.ardanil.aptitudetest.model

import com.squareup.moshi.Json

data class DetailBusinessResponse(

	@Json(name="hours")
	val hours: List<HoursItem>? = null,

	@Json(name="image_url")
	val image_url: String? = null,

	@Json(name="rating")
	val rating: Double? = null,

	@Json(name="coordinates")
	val coordinates: Coordinates? = null,

	@Json(name="review_count")
	val review_count: Int? = null,

	@Json(name="transactions")
	val transactions: List<Any?>? = null,

	@Json(name="photos")
	val photos: List<String?>? = null,

	@Json(name="url")
	val url: String? = null,

	@Json(name="is_claimed")
	val isClaimed: Boolean? = null,

	@Json(name="display_phone")
	val display_phone: String? = null,

	@Json(name="phone")
	val phone: String? = null,

	@Json(name="price")
	val price: String? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="alias")
	val alias: String? = null,

	@Json(name="location")
	val location: Location? = null,

	@Json(name="id")
	val id: String? = null,

	@Json(name="categories")
	val categories: List<CategoriesItem>? = null,

	@Json(name="is_closed")
	val isClosed: Boolean? = null
)
