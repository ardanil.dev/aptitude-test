package com.ardanil.aptitudetest.model

import com.squareup.moshi.Json

data class OpenItem(

	@Json(name="is_overnight")
	val isOvernight: Boolean? = null,

	@Json(name="start")
	val start: String? = null,

	@Json(name="end")
	val end: String? = null,

	@Json(name="day")
	val day: Int? = null
)
