package com.ardanil.aptitudetest.model

import com.squareup.moshi.Json

data class BusinessResponse(

	@Json(name="total")
	val total: Int? = null,

	@Json(name="region")
	val region: Region? = null,

	@Json(name="businesses")
	val businesses: List<BusinessesItem>? = null
)

data class Region(

	@Json(name="center")
	val center: Center? = null
)

data class Center(

	@Json(name="latitude")
	val latitude: Double? = null,

	@Json(name="longitude")
	val longitude: Double? = null
)


