package com.ardanil.aptitudetest.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Coordinates(

	@Json(name="latitude")
	val latitude: Double? = null,

	@Json(name="longitude")
	val longitude: Double? = null
) : Parcelable