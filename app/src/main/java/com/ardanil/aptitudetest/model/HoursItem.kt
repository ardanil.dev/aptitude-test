package com.ardanil.aptitudetest.model

import com.squareup.moshi.Json

data class HoursItem(

	@Json(name="is_open_now")
	val isOpenNow: Boolean? = null,

	@Json(name="hours_type")
	val hoursType: String? = null,

	@Json(name="open")
	val open: List<OpenItem>? = null
)
