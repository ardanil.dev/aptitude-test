package com.ardanil.aptitudetest.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoriesItem(

	@Json(name="alias")
	val alias: String? = null,

	@Json(name="title")
	val title: String? = null
) : Parcelable
