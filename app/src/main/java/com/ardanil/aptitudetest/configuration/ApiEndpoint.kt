package com.ardanil.aptitudetest.configuration

import com.ardanil.aptitudetest.model.BusinessResponse
import com.ardanil.aptitudetest.model.DetailBusinessResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiEndpoint {

    @GET(ApiKeys.SEARCH_BUSINESS)
    fun getSearchBusiness(
        @Header("Authorization") token: String,
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double,
        @Query("term") term: String?,
        @Query("location") location: String?,
        @Query("categories") categories: String?,
        @Query("sort_by") sort: String,
        @Query("limit") limit: Int
    ): Call<BusinessResponse>

    @GET(ApiKeys.DETAIL_BUSINESS)
    fun getDetailBusiness(
        @Header("Authorization") token: String,
        @Path("id") id: String
    ): Call<DetailBusinessResponse>

}