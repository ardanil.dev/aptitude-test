package com.ardanil.aptitudetest.configuration

object ApiKeys {

    const val SEARCH_BUSINESS = "businesses/search"
    const val DETAIL_BUSINESS = "businesses/{id}"

}