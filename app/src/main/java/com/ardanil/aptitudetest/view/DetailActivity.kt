package com.ardanil.aptitudetest.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.ardanil.aptitudetest.R
import com.ardanil.aptitudetest.viewmodel.DetailActivityVM
import com.ardanil.aptitudetest.viewmodel.DetailActivityVMF
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), OnMapReadyCallback {

	private var stringId: String? = null
	private lateinit var viewModel: DetailActivityVM
	private var gMap: GoogleMap? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_detail)
		stringId = intent.getStringExtra(BUSINESS_EXTRA)

		val viewModelFactory = DetailActivityVMF()
		viewModel = ViewModelProvider(this, viewModelFactory)[DetailActivityVM::class.java]
		initViews()
		observeLiveData()
	}

	private fun initViews() {
		val mapFragment = supportFragmentManager
			.findFragmentById(R.id.map) as SupportMapFragment
		mapFragment.getMapAsync(this)
	}

	@SuppressLint("SetTextI18n")
	private fun observeLiveData() {

		viewModel.isError().observe(this, {
			if (it != null) {
				when (it) {
					401 -> {
						showSnackbar("Oops..! You are unauthorized to access this content")
					}
					403 -> {
						showSnackbar("Oops..! API rate limit exceeded, Try Again Later")
					}
					else -> {
						showSnackbar("Oops..! Something Went Wrong")
					}
				}
			}
		})

		viewModel.getSearchBusiness().observe(this, {
			if (it != null) {
				tvBusinessName.text = it.name
				tvTotalReview.text = "${it.review_count} Reviews"
				it.rating?.let { rate -> rbStar.rating = rate.toFloat() }
				tvAddress.text = it.location?.display_address?.joinToString()
				tvPhone.text = it.display_phone
				val catItem = it.categories?.map { item -> item.title }
				tvCategories.text = catItem?.joinToString()
				Glide.with(this)
					.load(it.image_url)
					.centerCrop()
					.into(ivMainImage)
				it.hours?.map { hourItem ->
					hourItem.open?.map { openItem ->
						val renderer = HoursViewHolder.newInstance(flxHours)
						renderer.render(openItem)
						renderer.itemView
					}?.forEach(flxHours::addView)
				}
				val lat = it.coordinates?.latitude ?: 0.0
				val long = it.coordinates?.longitude ?: 0.0
				gMap?.clear()
				gMap?.addMarker(
					MarkerOptions()
						.position(LatLng(lat, long))
						.title(it.name)
				)
				gMap?.animateCamera( CameraUpdateFactory.newLatLngZoom( LatLng(lat, long), 17.0f ) )
			}
		})
	}

	private fun showSnackbar(s: String) {
		Snackbar.make(this.linearLayout, s, Snackbar.LENGTH_LONG).show()
	}

	companion object {
		const val BUSINESS_EXTRA = "BUSINESS_EXTRA"
	}

	override fun onMapReady(googleMap: GoogleMap) {
		gMap = googleMap
		stringId?.let { viewModel.fetchDetailBusiness(it) }
	}
}