package com.ardanil.aptitudetest.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.ardanil.aptitudetest.R
import com.ardanil.aptitudetest.viewmodel.MainActivityVM
import com.ardanil.aptitudetest.viewmodel.MainActivityVMF
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

	private lateinit var viewModel: MainActivityVM
	private var isLoading = false
	private lateinit var businessAdapter: BusinessAdapter
	private var selectedIndexSpinner: Int = 0

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val viewModelFactory = MainActivityVMF()
		viewModel = ViewModelProvider(this, viewModelFactory)[MainActivityVM::class.java]
		initViews()
		observeLiveData()
		viewModel.fetchSearchBusiness(null, null, null)

	}

	private fun initViews() {
		val gridLayoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
		rvItem.layoutManager = gridLayoutManager
		ArrayAdapter.createFromResource(
			this,
			R.array.category_array,
			android.R.layout.simple_spinner_item
		).also { adapter ->
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
			spCategory.adapter = adapter
		}
		rvItem.isNestedScrollingEnabled = true
		businessAdapter = BusinessAdapter()
		rvItem.adapter = businessAdapter
		spCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

			override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
				selectedIndexSpinner = p2
			}

			override fun onNothingSelected(p0: AdapterView<*>?) {
				//Nothing To Do
			}

		}

		btnFind.setOnClickListener {
			var term: String? = null
			var location: String? = null
			var categories: String? = null
			when(selectedIndexSpinner) {
				0 -> term = etSearch.text.toString()
				1 -> location = etSearch.text.toString()
				2 -> categories = etSearch.text.toString()
			}
			viewModel.fetchSearchBusiness(term, location, categories)
		}
	}

	@SuppressLint("NotifyDataSetChanged")
	private fun observeLiveData() {
		viewModel.isLoading().observe(this, {
			if (it != null) {
				isLoading = it
				this.progressBar.isVisible = it
			}
		})

		viewModel.isError().observe(this, {
			if (it != null) {
				when (it) {
					401 -> {
						showSnackbar("Oops..! You are unauthorized to access this content")
					}
					403 -> {
						showSnackbar("Oops..! API rate limit exceeded, Try Again Later")
					}
					else -> {
						showSnackbar("Oops..! Something Went Wrong")
					}
				}
			}
		})

		viewModel.getSearchBusiness().observe(this, {
			if (it != null) {
				it.businesses?.map { item ->
					Log.i("Business Name", item.name.toString())
				}
				it.businesses?.let { item ->
					businessAdapter.setItem(item)
					businessAdapter.notifyDataSetChanged()
				}

				it.total?.let { total ->
					rvItem.isVisible = total > 0
					llEmptyState.isVisible = total <= 0
				} ?: run {
					rvItem.visibility = View.GONE
					llEmptyState.visibility = View.VISIBLE
				}
			}
		})
	}

	private fun showSnackbar(s: String) {
		Snackbar.make(this.linearLayout, s, Snackbar.LENGTH_LONG).show()
	}
}