package com.ardanil.aptitudetest.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ardanil.aptitudetest.R
import com.ardanil.aptitudetest.model.OpenItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.hours_text_row.view.*
import java.text.DateFormatSymbols

class HoursViewHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    companion object {
        fun newInstance(
            parent: ViewGroup
        ): HoursViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.hours_text_row, parent, false)
            return HoursViewHolder(view)
        }
    }

    @SuppressLint("SetTextI18n")
    fun render(item: OpenItem) {
        item.day?.let {
            itemView.txtDay.text = DateFormatSymbols.getInstance().weekdays[it+1]
        }
        itemView.txtHour.text = "${item.start?.take(2)}:${item.start?.takeLast(2)} - ${item.end?.take(2)}:${item.end?.takeLast(2)}"
        /*txtDescription.isVisible = !isUseSmallText
        txtDescriptionSmall.isVisible = isUseSmallText
        if (isUseSmallText){
            txtDescriptionSmall.text = item
        } else {
            txtDescription.text = item
        }*/
    }
}
