package com.ardanil.aptitudetest.view

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ardanil.aptitudetest.R
import com.ardanil.aptitudetest.model.BusinessesItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.business_item.view.*
import java.util.*

class BusinessAdapter :
	RecyclerView.Adapter<BusinessAdapter.ViewHolder>() {
	private var business: List<BusinessesItem?>? = ArrayList<BusinessesItem>()
	override fun onCreateViewHolder(
		parent: ViewGroup,
		viewType: Int
	): ViewHolder {
		return ViewHolder(
			LayoutInflater.from(parent.context)
				.inflate(R.layout.business_item, parent, false)
		)
	}

	override fun onBindViewHolder(
		holder: ViewHolder,
		position: Int
	) {
		holder.bind(business?.get(position), position)
	}

	override fun getItemCount(): Int {
		return if (business != null) {
			business!!.size
		} else {
			0
		}
	}

	fun setItem(business: List<BusinessesItem?>?) {
		this.business = business
	}

	inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

		private var business: BusinessesItem? = null
		private var itemPosition = 0

		fun bind(business: BusinessesItem?, position: Int) {
			this.business = business
			itemPosition = position
			itemView.tvBusinessName.text = business?.name
			itemView.tvRating.text = business?.rating.toString()

			business?.location?.display_address?.let { locItem ->
				if (locItem.isNotEmpty()) {
					itemView.tvLocation.text = locItem[0]
				}
			}

			business?.image_url?.let {
				Glide.with(itemView)
					.load(it)
					.centerCrop()
					.into(itemView.ivBusiness)
			}

			itemView.setOnClickListener {
				itemView.context.startActivity(
					Intent(
						itemView.context,
						DetailActivity::class.java
					).putExtra(DetailActivity.BUSINESS_EXTRA, business?.id)
				)
			}
		}
	}

}