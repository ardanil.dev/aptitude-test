package com.ardanil.aptitudetest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ardanil.aptitudetest.BuildConfig
import com.ardanil.aptitudetest.configuration.ApiManager
import com.ardanil.aptitudetest.model.BusinessResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityVM : ViewModel() {

	private val loading = MutableLiveData<Boolean>()
	private val errorFetchUsers = MutableLiveData<Int?>()
	private val searchBusinessResponse: MutableLiveData<BusinessResponse?> =
		MutableLiveData()

	fun isLoading(): LiveData<Boolean> {
		return loading
	}

	fun isError(): LiveData<Int?> {
		return errorFetchUsers
	}

	fun getSearchBusiness(): LiveData<BusinessResponse?> {
		return searchBusinessResponse
	}

	private var getUsers: Call<BusinessResponse>? = null

	fun fetchSearchBusiness(term: String?, location: String?, categories: String?) {
		loading.value = true
		getUsers = ApiManager().getService()?.getSearchBusiness(
			token = "Bearer ${BuildConfig.TOKEN}",
			latitude = 37.786882,
			longitude = -122.399972,
			term = term,
			location = location,
			categories = categories,
			sort = "rating",
			limit = 30
		)
		getUsers?.enqueue(object : Callback<BusinessResponse?> {
			override fun onResponse(
				call: Call<BusinessResponse?>,
				response: Response<BusinessResponse?>
			) {
				loading.value = false
				if (response.code() == 200) {
					errorFetchUsers.value = null
					val responses: BusinessResponse? = response.body()
					searchBusinessResponse.setValue(responses)
				} else {
					errorFetchUsers.setValue(response.code())
				}
			}

			override fun onFailure(
				call: Call<BusinessResponse?>,
				t: Throwable
			) {
				loading.value = false
				errorFetchUsers.value = 500
			}
		})
	}
}
