package com.ardanil.aptitudetest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ardanil.aptitudetest.BuildConfig
import com.ardanil.aptitudetest.configuration.ApiManager
import com.ardanil.aptitudetest.model.DetailBusinessResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailActivityVM : ViewModel() {

	private val loading = MutableLiveData<Boolean>()
	private val errorFetchBusiness = MutableLiveData<Int?>()
	private val detailBusinessResponse: MutableLiveData<DetailBusinessResponse?> =
		MutableLiveData()

	fun isLoading(): LiveData<Boolean> {
		return loading
	}

	fun isError(): LiveData<Int?> {
		return errorFetchBusiness
	}

	fun getSearchBusiness(): LiveData<DetailBusinessResponse?> {
		return detailBusinessResponse
	}

	private var getDetail: Call<DetailBusinessResponse>? = null

	fun fetchDetailBusiness(id: String) {
		loading.value = true
		getDetail = ApiManager().getService()?.getDetailBusiness(
			token = "Bearer ${BuildConfig.TOKEN}",
			id = id
		)
		getDetail?.enqueue(object : Callback<DetailBusinessResponse?> {
			override fun onResponse(
				call: Call<DetailBusinessResponse?>,
				response: Response<DetailBusinessResponse?>
			) {
				loading.value = false
				if (response.code() == 200) {
					errorFetchBusiness.value = null
					val responses: DetailBusinessResponse? = response.body()
					detailBusinessResponse.setValue(responses)
				} else {
					errorFetchBusiness.setValue(response.code())
				}
			}

			override fun onFailure(
				call: Call<DetailBusinessResponse?>,
				t: Throwable
			) {
				loading.value = false
				errorFetchBusiness.value = 500
			}
		})
	}
}
